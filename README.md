## Gabriel Guzman's README (he/him)

Hi, I'm Gabriel Guzman, dev manager for the Loci team. Most people who know me
call me gabe. I've lived in California (mostly San Diego), New Jersey, Maryland,
Mexico D.F., Nevada, Montreal and now Dunham QC. I live on a small farm with my
partner, two children (8 and 5), 10 ducks, 12 chickens, two rats, and a cat.

I like to spend time gardening, reading, playing with my kids and playing with
computers. I also practice the Japanese martial art of Aikido, though since
having kids I haven't been active in that regard.

I speak English, French, and Spanish in that order of proficiency and enjoy
working in all of them. I will respond to you in whatever language you approach
me with.

I was a developer for most of my career, and though I still write code I stay
out of the developers' way. My experience is in backend web development and
platforming. I'm excited to get a chance to work on a platform that will support
such a large number of diverse engineers and technologies.

My tools of choice are OpenBSD, vim, Go, and all the base Unix utilities.

## Assumptions

- I assume everyone is working at least as hard as I am
- I assume everyone is doing the best they can with what they have
- I assume everyone's intentions are good

## Management style

I am a fan of [servant
leadership](https://en.wikipedia.org/wiki/Servant_leadership), I believe that I
work for the team and that my main job is to help you optimize for motivation.
In my favourite talk on the subject, Dan Pink mentions 3 things that drive
motivation: 

1. Autonomy - People want to feel they have some control over their destinies
2. Mastery - People want to feel they are improving their skills
3. Purpose - People want to feel they are contributing to something greater than
   themselves.

If you've never seen the video, I highly recommend it:
https://www.youtube.com/watch?v=7HwuJwrswiw

I try to be as transparent as possible, giving you all the information that I
have so you can adequately make decisions about your day to day work. I believe
in pushing control down the stack as close as possible to the people doing the
work.

I believe that problems should be surfaced as quickly as possible so they can be
discussed and resolved.

I understand that development work requires periods of deep uninterrupted focus,
and encourage you to block off chunks of your time in order to do this type of
work.

## How I work

### Communication

Async FTW. It's 2021, we live in a globally distributed always connected world.
The future of work is distributed and asynchronous. With that in mind, I prefer: 

1. emails over meetings
2. slack/teams chats over video calls
3. written documentation over interrupt driven support

That doesn't mean there isn't a time and place for synchronous communication,
just that I prefer to favour asynchronous communication.

The best ways to get in touch with me are: 

1. Slack/Teams
2. email
3. phone

I do my best to be offline when I'm not at work as I think it's important to set
a healthy example of work-life balance, if there's an emergency and you need to
get in touch, call me. My normal working hours are 8 a.m. eastern to 4 p.m. eastern
time, but I'm always available to help with production incidents.

If I message you outside of your normal working hours, please don't reply until
you start work again. I can wait. 

### Sometimes I forget stuff

While I do my best to write things down and maintain TODO lists of things I've
said I'd get done, sometimes things slip through the cracks. If you're
expecting something from me and I haven't gotten back to you in a reasonable
amount of time, please remind me, it's possible I just forgot. 

### 1:1s

1:1s for me are primarily about getting to know each other, and building up a
trust relationship. They are also great places to discuss struggles in our
personal lives, or work. I start with a once every 2 weeks cadence for 1:1s but
after the first few meetings, it's up to you how frequently you want to meet,
I'll also respect your "deep work" blocks of time and not schedule 1:1s during
those times. 

### I always have time for my team

Regardless of how busy I may seem, the team is my #1 priority, I will always
make time for you. Feel free to ping me anytime, schedule a meeting, send me a
message, etc. I'm here for you, really.

### Feedback

I like feedback. If there are things I could be doing better, please let me
know. I will do my best to also provide you with the feedback you need to get
better at what you do (I'm not great at this, but am actively working to
improve).

### Vacation requests

I like the "tell, don't ask" approach that gitlab takes on vacation. I trust you
to ensure that you won't be putting the team in the lurch, and that you'll
inform the team and myself ahead of time that you'll be taking time off. As long
as those things are met, I will just click the accept button on any vacation
requests I see.

## Things that have informed my management philosophy

[Netflix Culture Deck](https://jobs.netflix.com/culture)

- Trust is greater than control
- Push information down the stack

[Gitlab Handbook](https://about.gitlab.com/handbook/)

- Document everything
- Async ALL THE THINGS!

[Turn The Ship Around](https://davidmarquet.com/turn-the-ship-around-book/)

- Make everyone a leader
- The people closest to the work should have enough information to make good
  decisions.

[Radical Candor](https://www.radicalcandor.com/)

- Be wary of ruinous empathy
- Surface problems immediately
- Honesty is the best policy

[Julia Evans](https://jvns.ca/)
- What does a manager do
- How to work effectively with your manager
- [Help I have a manager](https://wizardzines.com/zines/manager/)
